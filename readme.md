## Step 1

Copy the below to a `/etc/apache2/sites-available/bookstack-new-domain.conf` file.
Then change the second line to be the correct domain.

```apache
<VirtualHost *:80>
  ServerName domain2.com

  ServerAdmin webmaster@localhost
  DocumentRoot /var/www/bookstack/public/

  <Directory /var/www/bookstack/public/>
      Options -Indexes +FollowSymLinks
      AllowOverride None
      Require all granted
      <IfModule mod_rewrite.c>
          <IfModule mod_negotiation.c>
              Options -MultiViews -Indexes
          </IfModule>

          RewriteEngine On

          # Handle Authorization Header
          RewriteCond %{HTTP:Authorization} .
          RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

          # Redirect Trailing Slashes If Not A Folder...
          RewriteCond %{REQUEST_FILENAME} !-d
          RewriteCond %{REQUEST_URI} (.+)/$
          RewriteRule ^ %1 [L,R=301]

          # Handle Front Controller...
          RewriteCond %{REQUEST_FILENAME} !-d
          RewriteCond %{REQUEST_FILENAME} !-f
          RewriteRule ^ index.php [L]
      </IfModule>
  </Directory>

  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```

## Step 2

Activate the above via `sudo a2ensite bookstack-new-domain.conf`.

## Step 3

Run certbot (`sudo certbot --apache`) and select to get a certificate for only domain2 (the new domain).
Do not extend an existing certificate. 

## Step 4

Edit bookstack `.env` file, and update `APP_URL` to use the new domain. This must still start with `https://`. 
If not done already, "Update System URL" command to convert usages of domain1 to domain2 (https://www.bookstackapp.com/docs/admin/commands/#update-system-url)